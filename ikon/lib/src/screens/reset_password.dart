import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'dart:convert';
import '../validation_mixin.dart';

class ResetPassword extends StatefulWidget {
  // values passed from retrieve_password page
  final String email;
  final String link;
  ResetPassword({Key key, this.email, this.link}) : super(key: key);

  @override
  _ResetPasswordState createState() => _ResetPasswordState();
}

class _ResetPasswordState extends State<ResetPassword> with ValidationMixin{
  final formkey = GlobalKey<FormState>(); //represents the formstate object
  bool _isLoading = false;
  bool _obscureText = true;
  String password = '';
  String prompt = '';
  String _warning;
  bool alert = false;

  submit(password) async{
    Map data = {'email': widget.email, 'password': password};
    var jsonData;
    http.Response response = await http.post("http://192.168.43.219:5000/apis/reset"+widget.link, body: data);
    if (response.statusCode < 200 || response.statusCode > 400 || json == null) {
        throw Exception("Could'nt connect, please ensure you have a stable network or further chrck ur PC IP adresss.");
    }
    else {
      jsonData = json.decode(response.body);
      String server_message = jsonData['message'];
      if(server_message.contains('reset')){
        setState(() {
          _warning = "Password reset successfully";
          print(_warning);
          alert = true;
         _isLoading = false;
         Navigator.pushNamed(context, "/Login"); 
        });
      } else {
        print(jsonData);
        setState(() {
          alert = true;
          print(server_message);
          _isLoading = false;  
        });
      }  
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        centerTitle: true,
        title: Text('Password Reset'),
        backgroundColor: Colors.red,
        elevation: 0,
        iconTheme:  IconThemeData(color: Colors.white),
      ),
      backgroundColor: Color(0xfff5f5f5),
      body: _body(context),
    ); 
  }

  Widget _body(BuildContext context){
    double screenHeight = MediaQuery.of(context).size.height;
    return Container(
      height: screenHeight,
      child: _isLoading ? Center(child: CircularProgressIndicator()) : ListView(
        shrinkWrap: true,
        padding: EdgeInsets.all(15.0),
        children: <Widget>[
          SizedBox(height: 140),
          _formUI()  
        ],
       ),
    );
  }

  Widget _formUI (){
    return Form( 
      key: formkey,                
      child: Container(
        padding: EdgeInsets.all(10.0),
        child: Column(
          children: <Widget>[
            Card(
              child: TextFormField( 
                keyboardType: TextInputType.emailAddress,
                obscureText: _obscureText,
                decoration: InputDecoration(
                  suffixIcon: GestureDetector(
                    child: Icon(
                      _obscureText ? Icons.visibility : Icons.visibility_off,
                    ),
                    onTap: (){
                      setState(() {
                        _obscureText = !_obscureText;
                      });
                    }
                  ),
                  prefixIcon: Icon(Icons.contact_mail),
                    labelText: 'New Password',
                    hintText: 'Enter your new password'
                ),
                validator: validatePassword,
                onSaved: (String value){
                  password = value;
                },
              )
            ),
            SizedBox(height: 0.0),
            Container(alignment: Alignment.center,
              child: Text(
                prompt,
                style: TextStyle(color: Colors.red, fontSize: 17, fontWeight: FontWeight.w600)
              ),
            ),
            Card(child:    
              SizedBox(width:350, height: 57,
                child: RaisedButton(
                  onPressed: (){
                    if(formkey.currentState.validate()){
                      formkey.currentState.save();
                      setState(() {
                        _isLoading = true;
                      });
                      submit(password);
                      formkey.currentState.reset();                         
                    } 
                  },
                  shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(5)
                  ),
                  padding: EdgeInsets.all(12),
                  color: Colors.red,
                  child: Text('Reset Password', style: TextStyle(color: Colors.white, fontSize: 20)),
                ),)
            ),
          ],
        ),
      ),
    );
  }
}

