from flask_jwt_extended import (
    create_access_token,
    create_refresh_token
)
from flask_restful import Resource
from . import status  # The status to import are in the same folder.
from .models import (UserModel,)
from .parser import (userParser, linkParser, accountParser)


class SignUp(Resource):
    @staticmethod
    def post():
        data = userParser.parse_args(strict=True)
        user = UserModel()
        user.fname = data['fname']
        user.mname = data['mname']
        user.lname = data['lname']
        user.phone = data['phone']
        user.email = data['email']
        user.password = UserModel.generate_hash(data['password'])
        user.save()
        return {'message': 'Registered successfully'}


class Login(Resource):
    @staticmethod
    def post():
        data = accountParser.parse_args(strict=True)
        query_results = UserModel.find_by_email(data['email'])
        user = None
        for i in query_results:
            user = i
            break
        if not user:
            return {'message': "User {} doesn't exist".format(data['email'])}

        if UserModel.verify_hash(data['password'], user.password):
            access_token = create_access_token(identity=user.email)
            refresh_token = create_refresh_token(identity=user.email)
            return {
                'message': 'True',
                'state': 'Logged in as {} {}'.format(user.fname, user.mname, user.lname),
                'access_token': access_token,
                'refresh_token': refresh_token
            }

        else:
            return {'message': 'Invalid credentials'}


class LinkGeneration (Resource):
    @staticmethod
    def post():
        data = linkParser.parse_args(strict=True)
        link = UserModel()
        link.email = data['email']  # Request for email for verification in the user table
        query_set = UserModel.find_by_email(link.email)
        user = None
        for i in query_set:
            user = i
            break
        if not user:
            return {"link": "No user with the entered email exist"}
        token = create_access_token(identity=user.email)  # Token makes it unique by hiding data
        user.token = token[:20]  # Truncate the first 20 of the token
        user.save()
        return {"link": "/"+user.token}


class AccountRecovery(Resource):  # This endpoint will be called automatically if temporary link is used
    @staticmethod
    def post(token):
        data = accountParser.parse_args(strict=True)
        recover = UserModel()
        recover.email = data['email']  # it will be automatically passed
        recover.password = UserModel.generate_hash(data['password'])  # newly set password
        query_set = UserModel.find_by_email(recover.email)
        customers = []
        for i in query_set:
            customers.append(i)
        if len(customers) == 0:
            return {"error": "No customer with the entered email exist"}
        customer = customers[0]
        token_verify = UserModel.find_by_token(token)
        user = None
        for i in token_verify:
            user = i
            break
        if not user:
            return {"error": "Don't try to be smart, Check the link received!"}
        customer.password = recover.password
        customer.save()
        return {"message": "Password reset. Keep your password safe!!!"}
