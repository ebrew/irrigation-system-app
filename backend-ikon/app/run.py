from flask_jwt_extended import JWTManager
from app import app, api, db
from app.resources import (SignUp, Login, AccountRecovery, LinkGeneration)


@app.before_first_request
def create_table():
    db.create_all()


jwt = JWTManager(app)


# mobile APIs
api.add_resource(Login, '/apis/mobile/client/login')  # User Login
api.add_resource(SignUp, '/apis/mobile/client/signup')  # User Sign Up
api.add_resource(AccountRecovery, '/apis/reset/<token>') # Use link sent to email to reset password
api.add_resource(LinkGeneration, '/apis/mobile/client/password_recovery/reset') # Generate link to be sent to user email

# Entry point
# Assign ur pc IPv4 address to the 'host'and then update all the ip address in the frontend
if __name__ == "__main__":
    app.run(host='192.168.43.219', port=5000, debug=True)
    # This tells the operating system to listen on a public IP.
