from passlib.hash import pbkdf2_sha256 as sha256
from datetime import datetime
from itsdangerous import TimedJSONWebSignatureSerializer as Serializer
from sqlalchemy import ForeignKey, Column, DateTime
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import relationship
from app import db, app

Base = declarative_base()


class BaseModel(db.Model):
    __abstract__ = True

    def save(self):
        db.session.add(self)
        db.session.commit()


class UserModel(BaseModel):
    __tablename__ = 'user'

    fname = db.Column(db.String(15))
    mname = db.Column(db.String(15))
    lname = db.Column(db.String(15))
    phone = db.Column(db.String(13), unique=True)
    email = db.Column(db.String(40), primary_key=True)
    password = db.Column(db.String(100))
    app_token = db.Column(db.Integer, nullable=True)
    token = db.Column(db.String(40), nullable=True)

    @classmethod
    def find_by_token(cls, token): # A method to search for a token to grant access for password reset
        return cls.query.filter_by(token=token)

    @classmethod
    def find_by_email(cls, email): # A method to search for a user using email
        return cls.query.filter_by(email=email)

    @staticmethod
    def generate_hash(password):
        return sha256.hash(password)

    @staticmethod
    def verify_hash(password, hash):
        return sha256.verify(password, hash)